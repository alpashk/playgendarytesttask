﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class PlayerMove : MonoBehaviour
{
    private Transform player;
    private GameMaster GM;
    private SoundManager SM;
    private DeathManager DM;
    private bool canMove = true;
    private bool onLog = false;
    public float jumpDuration { get; private set; } = 0.5f;
    private int layerMask;

   
    public void CannotMove()
    {
        canMove = false;
    }
    
    void Start()
    {
        player = this.transform;
        layerMask = 1 << 12;
        layerMask = ~layerMask;
        SM = GetComponent<SoundManager>();
        DM = GetComponent<DeathManager>();
        GM = GameObject.Find("GameMaster").GetComponent<GameMaster>();
        GM.GameBeg();
    }

    void ScoreManager()
    {
        if (player.position.z > GM.currentScore)
        {
            GM.AddScore();
        }
    }
 
    public void GoForward()
    {
        if (canMove)
        {
            GoZ(1);
        }
    }
    public void GoBackward()
    {
        if (canMove)
        {
            GoZ(-1);
        }
    }
    public void GoLeft()
    {
        if (canMove)
        {
            GoX(-1);
        }
    }
    public void GoRight()
    {
        if (canMove)
        {
            GoX(1);
        }
    }



    void Turn(float degree)
    {
        player.DORotate(new Vector3(0, degree, 0), .5f);
    }

    void GoX(float distance)
    {
        canMove = false;
        if (distance > 0)
        {
            Turn(90);
        }
        else if (distance < 0)
        {
            Turn(-90);
        }
        int whatInFront = WhatIsInDirection(new Vector3(distance, 0, 0));

        if (whatInFront == 11)
        {
            Debug.Log("Death");
            canMove = true;
            return;
        }

        else if (whatInFront == 10 || (player.position.x == -4 && distance == -1) || (player.position.x == 4 && distance == 1))
        {
            canMove = true;
            return;
        }

        SM.PlayMoveSound();
        player.DOJump(player.position + new Vector3(distance, 0, 0), 1f, 1, jumpDuration).SetEase(Ease.Linear).OnComplete(() => IsItWater());
    }
    void GoZ(float distance)
    {
        canMove = false;
        if (distance > 0)
        {
            Turn(0);
        }
        else if (distance < 0)
        {
            Turn(180);
        }
        Vector3 directionalVector = new Vector3(0, 0, distance);
        int whatInFront = WhatIsInDirection(directionalVector);
        if (whatInFront == 11)
        {

            canMove = false;
            DM.RanIntoAcar(directionalVector);
            return;
        }
        else if (whatInFront == 10)
        {
            canMove = true;
            return;
        }
        else if (whatInFront == 9)
        {
            onLog = true;
            ChangeParent(directionalVector);
        }
        else if (onLog)
        {
            onLog = false;
            gameObject.transform.parent = null;
        }


        Vector3 newPos;
        newPos.x = (float)Math.Round(player.localPosition.x, MidpointRounding.AwayFromZero); //округление для того, что бы в случае прыжка с бревна/на бревно не сбивалась решётка
        newPos.y = player.localPosition.y;
        newPos.z = player.localPosition.z + distance;
        SM.PlayMoveSound();
        player.DOLocalJump(newPos, 1f, 1, jumpDuration).SetEase(Ease.Linear).OnComplete(() => IsItWater());

    }
    void IsItWater()
    {

        int downLayer = WhatIsInDirection(Vector3.down);
        if (downLayer == 4)
        {
            DM.Drowned();
            return;
        }
        canMove = true;
        ScoreManager();
    }

    void ChangeParent(Vector3 direction)
    {
        RaycastHit CheckSpace;
        if (Physics.Raycast(player.position, direction, out CheckSpace, 1f, layerMask))
        {

            gameObject.transform.parent = CheckSpace.collider.gameObject.transform.parent;//убрать .parent если будут модельки
        }
    }


    int WhatIsInDirection(Vector3 direction)
    {
        RaycastHit CheckSpace;
        if (Physics.Raycast(player.position, direction, out CheckSpace, 1f, layerMask))
        {
            return CheckSpace.collider.gameObject.layer;
        }
        else return -1;
    }



    

}

