﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MoveStuff : MonoBehaviour
{
    public bool Cars = false;
    
       
    public int MoveDirection = 1;
    public float speed = 5f;
    private int startpos = 15;
    private float distanceBetweenStuff = 1;
    [SerializeField] float minDistanceBetween=3;
    [SerializeField] float maxDistanceBetween=6;
    [SerializeField] GameObject coin=null;

    private float minLength;
    private float totalLength;
    [SerializeField] List<GameObject> StuffList = null;
    public List<Transform> SpawnedStuff;

    void Start()
    {
        minLength = 2 * startpos;
        totalLength = 0;
        SpawnedStuff = new List<Transform>();
        distanceBetweenStuff = Random.Range(minDistanceBetween, maxDistanceBetween);
        SpawnStuff();
    }

    void SpawnStuff()
    {

        int lowerBorder = 0;
        int carY = 1;
        if (!Cars)
        {
            carY = 0;
            if (transform.position.z <= 100)
            {
                lowerBorder = 1;
            }
        }
        var i = 0;
        bool coinSpawned = false;
        while (totalLength < minLength)
        {
            SpawnedStuff.Add(Instantiate(StuffList[Random.Range(lowerBorder, StuffList.Count)],
                new Vector3(MoveDirection * startpos, transform.position.y+carY, transform.position.z), Quaternion.identity, transform).transform);
            totalLength += SpawnedStuff[i].GetChild(0).gameObject.GetComponent<MeshFilter>().mesh.bounds.size.x * SpawnedStuff[i].GetChild(0).localScale.x;//добавляем длину созданного бревна
            totalLength += distanceBetweenStuff;//добавляем расстояние между элементами



            if(Cars && MoveDirection<0)
            {
                SpawnedStuff[i].GetChild(0).rotation = Quaternion.Euler(0, 0, 0);
            }
            else if(!Cars && !coinSpawned && Random.Range(0, 21)%20==0)
            {
                coinSpawned = true;
                var spawnedCoin = Instantiate(coin, SpawnedStuff[i]);
                coin.transform.position = new Vector3(0, 0.6f, 0);
            }
            i++;

        }
        SendStuff();
    }

    public void SendStuff()
    {
        StartCoroutine(StuffSend());
    }

    private IEnumerator StuffSend()
    {

        TweenParams tParms = new TweenParams().SetLoops(-1, LoopType.Restart).SetAutoKill(false).SetSpeedBased().SetEase(Ease.Linear);
        SpawnedStuff[0].DOLocalMoveX(MoveDirection * (startpos - totalLength), speed).SetAs(tParms);

        for (var i = 1; i < SpawnedStuff.Count; i++)
        {

            float prevLength = SpawnedStuff[i-1].GetChild(0).gameObject.GetComponent<MeshFilter>().mesh.bounds.size.x * SpawnedStuff[i-1].GetChild(0).localScale.x;
            float curLength = SpawnedStuff[i].GetChild(0).gameObject.GetComponent<MeshFilter>().mesh.bounds.size.x * SpawnedStuff[i].GetChild(0).localScale.x;
            float waitTime = (SpawnedStuff[i - 1].GetChild(0).localScale.x / 2 + distanceBetweenStuff + SpawnedStuff[i].GetChild(0).localScale.x / 2) / speed;
            yield return new WaitUntil(() => System.Math.Abs(SpawnedStuff[i - 1].position.x - SpawnedStuff[i].position.x) - prevLength / 2 - curLength / 2 > distanceBetweenStuff);

            SpawnedStuff[i].DOLocalMoveX(MoveDirection * (startpos - totalLength), speed).SetAs(tParms);

        }

    }

    private void OnDestroy()
    {
        for (var i = 0; i < SpawnedStuff.Count; i++)
        {
            SpawnedStuff[i].DOPause();
        }
    }

}
