﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class DeathManager : MonoBehaviour
{
    private Transform player;
    private int layerMask;
    private SoundManager SM;
    private GameMaster GM;
    private PlayerMove PM;
    private bool dead = false;
    [SerializeField] GameObject Eagle = null;
    void Start()
    {
        player = this.transform;
        layerMask = 1 << 12;
        layerMask = ~layerMask;
        SM = GetComponent<SoundManager>();
        PM = GetComponent<PlayerMove>();

        GM = GameObject.Find("GameMaster").GetComponent<GameMaster>();
    }

    void GameEnd()
    {
        DOTween.PauseAll();
        GM.EndGame();
        SceneManager.LoadScene("EndGame");
    }


    public void RanIntoAcar(Vector3 direction)
    {
        dead = true;
        RaycastHit CheckSpace;
        if (Physics.Raycast(player.position, direction, out CheckSpace, 1f, layerMask))
        {
            SM.PlayDeathSound();
            gameObject.transform.parent = CheckSpace.collider.gameObject.transform.parent;
            Vector3 hitPosition = CheckSpace.point - player.parent.position;
            player.DOLocalJump(hitPosition, .5f, 1, .3f);
            player.DOScale(new Vector3(1f, 1f, 0.1f), .3f).OnComplete(() => StartCoroutine(WaitTillEnd(.6f)));

        }
    }
    IEnumerator WaitTillEnd(float waittime)
    {
        yield return new WaitForSeconds(waittime);
        GameEnd();
    }

    public void RanOver()
    {
        if (!dead)
        {
            SM.PlayDeathSound();
            dead = true;
            PM.CannotMove();
            player.DOPause();
            player.DOMoveY(.55f, 0.1f);
            player.DOScale(new Vector3(1f, 0.1f, 1f), .3f).OnComplete(() => StartCoroutine(WaitTillEnd(.6f)));
        }
    }

    public void OutOfBounds()
    {
        if (!dead)
        {
            dead = true;
            PM.CannotMove();
            player.DOPause();
            GameObject gO = Instantiate(Eagle, new Vector3(transform.position.x, transform.position.y + 3, transform.position.z + 17), Quaternion.identity);
            SM.PlayEagleSound();
            gO.transform.DOMove(new Vector3(transform.position.x, transform.position.y + 1, transform.position.z), .4f).OnComplete(() => OnOutOfBoundsComplete(gO));
        }
    }
    private void OnOutOfBoundsComplete(GameObject eagle)
    {
        transform.parent = eagle.transform;
        eagle.transform.DOMove(new Vector3(transform.position.x, transform.position.y + 1, transform.position.z - 5), .5f).OnComplete(() => GameEnd());
    }

    public void Drowned()
    {
        if (!dead)
        {

            dead = true;
            PM.CannotMove();
            SM.PlayDrownSound();
            player.DOPause();
            player.DOMoveY(-0.2f, .3f).OnComplete(() => GameEnd());
        }
    }
}


