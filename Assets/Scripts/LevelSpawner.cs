﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSpawner : MonoBehaviour
{
    private Transform player;
    private float currentZcoord;

    [SerializeField] private List<GameObject> StartingLocation = null;
    [SerializeField] private List<GameObject> GrassLocations = null;
    [SerializeField] private List<GameObject> WaterWithLillies = null;
    [SerializeField] private GameObject Road = null;
    [SerializeField] private GameObject railRoad = null;
    [SerializeField] private GameObject Water=null;
    [SerializeField] private GameObject coinPrefab = null;

    private List<GameObject> spawnedLines;

    public float DistanceToSpawningObjects = 10;
    public float DistanceToDeleteObjects = 5;
    public float StartZcoord = -3;
    
    void Start()
    {
        spawnedLines = new List<GameObject>();
        currentZcoord = StartZcoord;
        player = GameObject.Find("Player").transform;
        foreach (var gO in StartingLocation)
        {
            spawnedLines.Add(Instantiate(gO, new Vector3(0, 0, currentZcoord), Quaternion.identity));
            currentZcoord++;
        }
        spawnedLines.Add(Instantiate(GrassLocations[Random.Range(0, GrassLocations.Count)], new Vector3(0, 0, currentZcoord), Quaternion.identity));
        currentZcoord++;


        while (player.position.z + DistanceToSpawningObjects < currentZcoord)
        {
            RandomizeNext();
        }


    }

    void FixedUpdate()
    {
        if (player.position.z + DistanceToSpawningObjects > currentZcoord)
        {
            RandomizeNext();
            DeletePrev();
        }
    }

    private void RandomizeNext()
    {
        int choiseManager = Random.Range(2, 17);
        if (choiseManager % 3 == 0)
        {        
            SpawnObj(GrassLocations[Random.Range(0, GrassLocations.Count)]);
            //spawn ground
        }
        else if (choiseManager%4 ==0 )
        {
            if(currentZcoord>15)
            SpawnWater();
            //spawn water
        }
        else if(choiseManager % 2 == 0)
        {
            SpawnRoad();
            //spawn road
        }
        else if (choiseManager % 5 == 0)
        {
            //spawn railroad
            SpawnCoin();
            SpawnObj(railRoad);
        }
        else if (choiseManager % 7 == 0)
        {
            //spawn lillies
            if (currentZcoord > 15)
                SpawnObj(WaterWithLillies[Random.Range(0, WaterWithLillies.Count)]);
        }
        else if (choiseManager % 11 == 0)
        {
            if (currentZcoord > 15)
            {
                SpawnObj(WaterWithLillies[Random.Range(0, WaterWithLillies.Count)]);
                var num = Random.Range(4, 8);
                for (var i = 0; i < num; i++)
                {
                    var decider = Random.Range(0, 6);
                    if (decider % 5 == 0)
                    {
                        SpawnObj(WaterWithLillies[Random.Range(0, WaterWithLillies.Count)]);
                    }
                    else
                    {
                        SpawnWater();
                    }
                }
                SpawnObj(GrassLocations[Random.Range(0, GrassLocations.Count)]);
            }
            //spawn lots of water
        }
        else if(choiseManager%13 == 0)
        {
            SpawnObj(GrassLocations[Random.Range(0, GrassLocations.Count)]);
            var num = Random.Range(4, 8);
            for (var i = 0; i < num; i++)
            {
                var decider = Random.Range(0, 6);
                if (decider % 5 == 0)
                {
                    SpawnObj(GrassLocations[Random.Range(0, GrassLocations.Count)]);
                }
                else
                {
                    SpawnRoad();
                }
            }
            SpawnObj(GrassLocations[Random.Range(0, GrassLocations.Count)]);
            //spawn many roads
        }
    }
    private void SpawnWater()
    {
        float speed = Random.Range(1.5f, 3f);
        int flow;
        if (Random.Range(0, 100) % 2 == 0)
        {
            flow = -1;
        }
        else
        {
            flow = 1;
        }
        MoveStuff tmp = Instantiate(Water, new Vector3(0, 0, currentZcoord), Quaternion.identity).GetComponent<MoveStuff>();
        spawnedLines.Add(tmp.gameObject);
        tmp.speed = speed;
        tmp.MoveDirection = flow;
        currentZcoord++;
    }
    private void SpawnRoad()
    {
        SpawnCoin();
        float speed = Random.Range(4f, 6.5f);
        int flow;
        if (Random.Range(0, 100)%2 == 0)
        {
            flow = -1;
        }
        else
        {
            flow = 1;
        }
        MoveStuff tmp = Instantiate(Road, new Vector3(0, 0, currentZcoord), Quaternion.identity).GetComponent<MoveStuff>();

        spawnedLines.Add(tmp.gameObject);
        tmp.speed = speed;
        tmp.MoveDirection = flow;
        currentZcoord++;
    }

    private void SpawnCoin()
    {
        if(Random.Range(0, 101) % 4 == 0)
        {
            Instantiate(coinPrefab, new Vector3(Random.Range(-4, 5), .6f, currentZcoord), Quaternion.Euler(90, 0, 0));
        }
    }

    private void SpawnObj(GameObject objToSpawn)
    {
        spawnedLines.Add(Instantiate(objToSpawn, new Vector3(0, 0, currentZcoord), Quaternion.identity));
        currentZcoord++;
    }
    private void DeletePrev()
    {
        int n = spawnedLines.Count;
        for(var i =0; i<n; i++)
        {
            if (player.position.z - DistanceToDeleteObjects >= spawnedLines[i].transform.position.z)
            {
                Destroy(spawnedLines[i]);
                spawnedLines.RemoveAt(i);
                i--;
                n--;
            }
            else return;
        }
    }
}
