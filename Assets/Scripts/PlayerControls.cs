﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControls : MonoBehaviour
{
    private PlayerMove move;

    private Vector2 firstPressPos;
    private Vector2 secondPressPos;
    private Vector2 currentSwipe;

    public float swipeLength = 0.4f;
    void Start()
    {
        move = GetComponent<PlayerMove>();
    }


    void Update()
    {
        if (Time.timeScale != 0)
        {
            if (Input.GetMouseButtonDown(0))
            {
                firstPressPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            }
            if (Input.GetMouseButtonUp(0))
            {
                secondPressPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
                currentSwipe = new Vector2(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);
                currentSwipe.Normalize();



                //swipe upwards
                if (currentSwipe.y >= 0 && currentSwipe.x > -swipeLength && currentSwipe.x < swipeLength)
                {
                    move.GoForward();
                }
                //swipe down
                if (currentSwipe.y < 0 && currentSwipe.x > -swipeLength && currentSwipe.x < swipeLength)
                {
                    move.GoBackward();
                }
                //swipe left
                if (currentSwipe.x < 0 && currentSwipe.y > -swipeLength && currentSwipe.y < swipeLength)
                {
                    move.GoLeft();
                }
                //swipe right
                if (currentSwipe.x > 0 && currentSwipe.y > -swipeLength && currentSwipe.y < swipeLength)
                {
                    move.GoRight();
                }
            }
        }
    }
}
