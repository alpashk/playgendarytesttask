﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CameraFollow : MonoBehaviour
{
    private Transform player;
    private float maxSpeed;
    private float minSpeed;
    private float speedPercent;
    public float minDistancePerSecond=.5f;
    public float maxDistance = 6f;
    public float minDistance = 1f;
    private float middleDistance;
    
    void Start()
    {
        middleDistance = (maxDistance + minDistance) / 2;
        minSpeed = minDistancePerSecond / 50;   
        player = GameObject.Find("Player").transform;
        maxSpeed =1/ player.gameObject.GetComponent<PlayerMove>().jumpDuration/50;//Игрок передвигается на 1 юнит за jumpDuration, при FixedUpdate в секунду происходит 50 вычислений, максимальная скорость - 1/jumpduration/50
        speedPercent = (maxSpeed - minSpeed) / 100;
    }

    void FixedUpdate()
    {
        //
        if (player.position.z - transform.position.z > maxDistance)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z +maxSpeed);
        }
        else if (player.position.z - transform.position.z > middleDistance) //Скорость должна постепенно расти от minSpeed до maxSpeed;
        {
            var distancePercent = ((player.position.z - transform.position.z - middleDistance) / (maxDistance - middleDistance))*100;
            transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + minSpeed +(speedPercent* distancePercent));
        }
        else if (player.position.z-transform.position.z>minDistance) //Скорость постоянна и равна minSpeed;
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z +minSpeed);
        }
        else
        {
            transform.DOMoveZ(player.transform.position.z - middleDistance, 1 / (maxSpeed * 25));
            player.GetComponent<DeathManager>().OutOfBounds();
            this.enabled = false;
        }

        if(player.position.x>=-3 && player.position.x <= 3)
        {
            transform.position = new Vector3(player.position.x+1, transform.position.y, transform.position.z);
        }

    }
}
