﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundControl : MonoBehaviour
{
    void Start()
    {
        UpdateSound();
    }

    public void UpdateSound()
    {
        if (PlayerPrefs.GetInt("Sound", 1) == 1)
        {
            gameObject.GetComponent<AudioListener>().enabled = true;
        }
        else
        {
            gameObject.GetComponent<AudioListener>().enabled = false;
        }
    }
}
