﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarTexture : MonoBehaviour
{

    [SerializeField] private List<Material> possibleMaterials=null;
    void Start()
    {
        GetComponent<MeshRenderer>().material = possibleMaterials[Random.Range(0, possibleMaterials.Count)];
    }
}
