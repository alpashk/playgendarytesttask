﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinScript : MonoBehaviour
{
    GameMaster GM;

    void Start()
    {
        GM = GameObject.Find("GameMaster").GetComponent<GameMaster>();
    }

    void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.layer == 12)
        {
            col.gameObject.GetComponent<SoundManager>().PlayCoinSound();
            GM.AddCoin();
            Destroy(gameObject);
        }
    }

}
