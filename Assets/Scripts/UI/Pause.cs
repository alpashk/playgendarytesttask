﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pause : MonoBehaviour
{
    GameMaster GM;
    bool isUnpausing = false;
    [SerializeField] GameObject PauseScreen = null;
    private Text pauseText;
    void Start()
    {
        GM = GameObject.Find("GameMaster").GetComponent<GameMaster>();
        pauseText = PauseScreen.transform.GetChild(0).GetComponent<Text>();
        PauseScreen.SetActive(false);
    }

    public void Pasue()
    {

        if (Time.timeScale == 0 && !isUnpausing)
        {
            isUnpausing = true;
            StartCoroutine(Unpause());
        }
        else if (!isUnpausing)
        {
            PauseScreen.SetActive(true);
            pauseText.gameObject.GetComponent<TextManager>().UpdateLanguage();
            GM.Pause();
        }
    }

    IEnumerator Unpause()
    {
        pauseText.text = 3.ToString();
        yield return new WaitForSecondsRealtime(1f);
        pauseText.text = 2.ToString();
        yield return new WaitForSecondsRealtime(1f);
        pauseText.text = 1.ToString();
        yield return new WaitForSecondsRealtime(1f);
        PauseScreen.SetActive(false);
        isUnpausing = false;
        GM.Pause();

    }


}
