﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonSound : MonoBehaviour
{
    private AudioSource audSor;
    [SerializeField] AudioClip sound=null;
    void Start()
    {
        audSor = gameObject.AddComponent<AudioSource>();
        audSor.clip = sound;
        audSor.playOnAwake = false;
        gameObject.GetComponent<Button>().onClick.AddListener(()=>PlaySound());
    }

    public void PlaySound()
    {
        if(audSor.isPlaying)
        {
            audSor.Stop();
        }
        audSor.PlayOneShot(sound);
    }
}
