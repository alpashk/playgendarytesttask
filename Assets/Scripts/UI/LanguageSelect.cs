﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LanguageSelect : MonoBehaviour
{
    [SerializeField] List<Sprite> possibleLanguages= null;
    private GameMaster GM;
    private Image png;

    void Start()
    {
        GM = GameObject.Find("GameMaster").GetComponent<GameMaster>();
        png = GetComponent<Image>();
        if(PlayerPrefs.GetInt("Language", 0)>possibleLanguages.Count)
        {
            PlayerPrefs.SetInt("Language", 0);
        }
        png.sprite = possibleLanguages[PlayerPrefs.GetInt("Language", 0)];
    }
    
    public void NextLanguage()
    {
        int currentLanguage = PlayerPrefs.GetInt("Language", 0);
        if(currentLanguage+1<possibleLanguages.Count)
        {
            PlayerPrefs.SetInt("Language", currentLanguage+1);
            png.sprite = possibleLanguages[currentLanguage + 1];
            GM.ChangeLanguage();
        }
        else
        {
            PlayerPrefs.SetInt("Language", 0);
            png.sprite = possibleLanguages[ 0];
            GM.ChangeLanguage();
        }
    }
}
