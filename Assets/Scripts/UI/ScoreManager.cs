﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    [SerializeField] Text CoinText=null;
    [SerializeField] Text ScoreText=null;
    [SerializeField] Text HighScore = null;

    public  GameMaster GM;

    void Start()
    {

        GM = GameObject.Find("GameMaster").GetComponent<GameMaster>();
        UpdateScore();
        UpdateCoins();
        UpdateHighScore();
    }

    public void UpdateCoins()
    {
        if (CoinText != null)
        {
            CoinText.text = GM.Coins.ToString();
        }
    }

    public void UpdateScore()
    {
        if (ScoreText != null)
        {
            ScoreText.text = GM.currentScore.ToString();
        }
    }

    public void UpdateHighScore()
    {
        if (HighScore != null)
        {
            HighScore.text = GM.HighScore.ToString();
        }
    }


    IEnumerator FindRoutine()//иначе иногда возникают ошибки с ненахождением GM
    {
        yield return new WaitForFixedUpdate();
        GM = GameObject.Find("GameMaster").GetComponent<GameMaster>();
        UpdateScore();
        UpdateCoins();
        UpdateHighScore();
    }

}
