﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextManager : MonoBehaviour
{
    [SerializeField] int word = 0;
    public GameMaster GM;
    private Text txt;
    void Start()
    {
        txt = gameObject.GetComponent<Text>();

        StartCoroutine(FindRoutine());
    }

    public void UpdateLanguage()
    {
        if (word < GM.Localisation.Length)
        {
            txt.text = GM.Localisation[word];
        }
        else
        {
            txt.text = "Undef";
        }

    }

    private IEnumerator FindRoutine()//в случае поиска в старте иногда не находит GameMaster
    {
        yield return new WaitForFixedUpdate();
        GM = GameObject.Find("GameMaster").GetComponent<GameMaster>();

        UpdateLanguage();
    }

    void OnEnable()//onEnable включается при запуске сцены и провоцирует ошибки
    {
        StartCoroutine(FindRoutine());
    }

}
