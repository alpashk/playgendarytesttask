﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundButton : MonoBehaviour
{
    [SerializeField] GameObject disabled = null;
    private SoundControl SC;

    void Start()
    {
        SC = Camera.main.gameObject.GetComponent<SoundControl>();
        if(PlayerPrefs.GetInt("Sound", 1) == 0)
        {
            disabled.SetActive(true);
        }
        else
        {
            disabled.SetActive(false);
        }
    }

    public void Sound()
    {
        if (PlayerPrefs.GetInt("Sound", 1) == 0)
        {
            PlayerPrefs.SetInt("Sound", 1);
            disabled.SetActive(false);
            SC.UpdateSound();
        }
        else
        {
            PlayerPrefs.SetInt("Sound", 0);
            disabled.SetActive(true);
            SC.UpdateSound();
        }
    }
}
