﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class GameMaster : MonoBehaviour
{
    public int HighScore;
    public int currentScore { get; private set; }
    public int Coins { get; private set; }
    public string[] Localisation { get; private set; }

    private ScoreManager SM;


    void Start()
    {


        GameObject[] obj = GameObject.FindGameObjectsWithTag("GameController");
        if (obj.Length > 1)
        {
            Destroy(this.gameObject);
        }
        else 
        {
            DontDestroyOnLoad(this.gameObject);
            SetLanguage();
            HighScore = PlayerPrefs.GetInt("HighScore", 0);

            Coins = PlayerPrefs.GetInt("Coins", 0);

            SM = GameObject.Find("EventSystem").GetComponent<ScoreManager>();
        }
       

    }

    public void GameBeg()
    {
        currentScore = 0;
        SM = GameObject.Find("EventSystem").GetComponent<ScoreManager>();
        SM.UpdateScore();
    }

    public void Pause()
    {
        if(Time.timeScale ==1)
        {
            Time.timeScale = 0;
            DOTween.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
            DOTween.timeScale = 1;
        }
    }

    void SetLanguage()
    {
        int Language = PlayerPrefs.GetInt("Language", 0);
        var TextFile = Resources.Load<TextAsset>("Languages");
        Localisation = TextFile.text.Split('\n')[Language].Split('~');
    }

    public void ChangeLanguage()
    {
        SetLanguage();
        GameObject.FindGameObjectsWithTag("Text").ToList().ForEach(x => x.GetComponent<TextManager>().UpdateLanguage());
    }

    public void AddScore()
    {
        currentScore++;
        SM.UpdateScore();
        
    }

    public void AddCoin()
    {
        Coins++;
        if(SM == null)
        {
            SM = GameObject.Find("EventSystem").GetComponent<ScoreManager>();
        }
        SM.UpdateCoins();
    }

    public void EndGame()
    {
        if(HighScore<currentScore)
        {
            HighScore = currentScore;
            PlayerPrefs.SetInt("HighScore", HighScore);
        }
        PlayerPrefs.SetInt("Coins", Coins);
    }

}
