﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TrafficLight : MonoBehaviour
{
    private List<Light> Lights;


    void Start()
    {
        Lights = transform.GetComponentsInChildren<Light>().ToList();
        Lights.ForEach(x => x.enabled = false);
    }

    public void EnableLights(float enableTime)
    {

        StartCoroutine(DisableLights(enableTime));
    }

    IEnumerator DisableLights(float enableTime)
    {

        Lights.ForEach(x => x.enabled = true);
        yield return new WaitForSeconds(enableTime);
        Lights.ForEach(x => x.enabled = false);
    }
}
