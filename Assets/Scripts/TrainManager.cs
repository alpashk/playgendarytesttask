﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TrainManager : MonoBehaviour
{
    private TrafficLight TL;
    private AudioSource source;
    [SerializeField] Transform Train= null;
    public float speed = 15f;
    public float waitTime;
    public float lightsTime;

    void Start()
    {
        TL = GetComponentInChildren<TrafficLight>();
        StartCoroutine(TrainGoesChooo());
        source = GetComponent<AudioSource>();
    }

    IEnumerator TrainGoesChooo()
    {
        waitTime = Random.Range(2, 5);
        yield return new WaitForSeconds(waitTime);
        TL.EnableLights(lightsTime);
        yield return new WaitForSeconds(lightsTime);
        source.Play();
        Train.DOLocalMoveX(-26, speed).SetSpeedBased().SetEase(Ease.Linear).OnComplete(()=>CompletionFunc());
    }

    void CompletionFunc()
    {
        source.Stop();
        Train.DORewind();
        StartCoroutine(TrainGoesChooo());
    }

}

