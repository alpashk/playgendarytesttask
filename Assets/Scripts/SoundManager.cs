﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    private AudioSource source;
    [SerializeField] AudioClip moveSound = null;
    [SerializeField] AudioClip deathSound = null;
    [SerializeField] AudioClip drownSound = null;
    [SerializeField] AudioClip eagleSound = null;
    [SerializeField] AudioClip coinPickup = null;
    void Start()
    {
        source = GetComponent<AudioSource>();

    }

    public void PlayMoveSound()
    {
        if (source.isPlaying)
        {
            source.Stop();
        }
        source.PlayOneShot(moveSound);
    }
    
    public void PlayDeathSound()
    {
        if (source.isPlaying)
        {
            source.Stop();
        }
        source.PlayOneShot(deathSound);
    }
    public void PlayDrownSound()
    {
        if (source.isPlaying)
        {
            source.Stop();
        }
        source.PlayOneShot(drownSound);
    }
    public void PlayEagleSound()
    {
        if (source.isPlaying)
        {
            source.Stop();
        }
        source.PlayOneShot(eagleSound);
    }

    public void PlayCoinSound()
    {
        source.PlayOneShot(coinPickup);
    }
}
